/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Paul
 */
public class Players {
      StringProperty TEAM ;
      StringProperty LAST_NAME;
      StringProperty FIRST_NAME;
      StringProperty QP;
     IntegerProperty  AB;
     IntegerProperty  R;
     IntegerProperty  H;
     IntegerProperty  HR;
     IntegerProperty  RBI;
     IntegerProperty  SB;
     StringProperty NOTES;
     IntegerProperty YEAR_OF_BIRTH;
     StringProperty NATION_OF_BIRTH;
    
    public static final String DEFAULT_TEAM = "<ENTER TEAM>";
    public static final String DEFAULT_LAST_NAME = "<ENTER LAST_NAME>";
    public static final String DEFAULT_FIRST_NAME = "<ENTER FIRST_NAME>";
    public static final String DEFAULT_QP = "<ENTER QP>";
    public static final int DEFAULT_AB = 0; 
    public static final int DEFAULT_R = 0; 
    public static final int DEFAULT_H = 0; 
    public static final int DEFAULT_HR = 0; 
    public static final int DEFAULT_RBI = 0; 
    public static final int DEFAULT_SB = 0; 
    public static final String DEFAULT_NOTES = "<ENTER NOTES>";
    public static final int DEFAULT_YEAR_OF_BIRTH = 0; 
    public static final String DEFAULT_NATION_OF_BIRTH = "<ENTER NATION_OF_BIRTH>";
       
    
    
    public Players() {
        TEAM = new SimpleStringProperty(DEFAULT_TEAM);
        AB = new SimpleIntegerProperty(DEFAULT_AB);
    }
    
    public void reset() {
        setTEAM(DEFAULT_TEAM);
        setAB(DEFAULT_AB);
    }
    public String getTEAM() {
        return TEAM.get();
    }
    
    public void setTEAM(String initTopic) {
        TEAM.set(initTopic);
    }
    
    public StringProperty TEAMProperty() {
        return TEAM;
    }
    public String getLAST_NAME() {
        return LAST_NAME.get();
    }
    
    public void setLAST_NAME(String initTopic) {
        LAST_NAME.set(initTopic);
    }
    
    public StringProperty LAST_NAMEProperty() {
        return LAST_NAME;
    }
      public String getFIRST_NAME() {
        return FIRST_NAME.get();
    }
    
    public void setFIRST_NAME(String initTopic) {
        FIRST_NAME.set(initTopic);
    }
    
    public StringProperty FIRST_NAMEProperty() {
        return FIRST_NAME;
    }
    public String getQP() {
        return QP.get();
    }
    
    public void setQP(String initTopic) {
        QP.set(initTopic);
    }
    
    public StringProperty QPProperty() {
        return QP;
    }
     public int getAB() {
        return AB.get();
    }
    
    public void setAB(int initSessions) {
        AB.set(initSessions);
    }
    
    public IntegerProperty ABProperty() {
        return AB;
    }
    public int getR() {
        return R.get();
    }
    
    public void setR(int initSessions) {
        R.set(initSessions);
    }
    
    public IntegerProperty RProperty() {
        return R
                ;
    } public int getH() {
        return H.get();
    }
    
    public void setH(int initSessions) {
        H.set(initSessions);
    }
    
    public IntegerProperty HProperty() {
        return H;
    } public int getHR() {
        return HR.get();
    }
    
    public void setHR(int initSessions) {
        HR.set(initSessions);
    }
    
    public IntegerProperty HRProperty() {
        return HR;
    } 
    public int getRBI() {
        return RBI.get();
    }
    
    public void setRBI(int initSessions) {
        RBI.set(initSessions);
    }
    
    public IntegerProperty RBIProperty() {
        return RBI;
    } public int getSB() {
        return SB.get();
    }
    
    public void setSB(int initSessions) {
        SB.set(initSessions);
    }
    
    public IntegerProperty SBProperty() {
        return SB;
    }
    public String getNOTES() {
        return NOTES.get();
    }
    
    public void setNOTES(String initTopic) {
        NOTES.set(initTopic);
    }
    
    public StringProperty NOTESProperty() {
        return NOTES;
    }
    public int getYEAR_OF_BIRTH() {
        return YEAR_OF_BIRTH.get();
    }
    
    public void setYEAR_OF_BIRTH(int initSessions) {
        YEAR_OF_BIRTH.set(initSessions);
    }
    
    public IntegerProperty YEAR_OF_BIRTHProperty() {
        return YEAR_OF_BIRTH;
    }
    public String getNATION_OF_BIRTH() {
        return NATION_OF_BIRTH.get();
    }
    
    public void setNATION_OF_BIRTH(String initTopic) {
        NATION_OF_BIRTH.set(initTopic);
    }
    
    public StringProperty NATION_OF_BIRTHProperty() {
        return NATION_OF_BIRTH;
    }
}
